﻿namespace sliding_puzzle
{
    partial class Sliding_Puzzle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sliding_Puzzle));
            this.Sliding_Puzzle_MenuStrip = new System.Windows.Forms.MenuStrip();
            this.Sliding_Puzzle_Game_Menu_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_New_Game_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_1_Separator = new System.Windows.Forms.ToolStripSeparator();
            this.Sliding_Puzzle_Pause_Game_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_Continue_Game_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_2_Separator = new System.Windows.Forms.ToolStripSeparator();
            this.Sliding_Puzzle_Board_Size_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_Small_Size_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_Medium_Size_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_Large_Size_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_Scramble_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_3_Separator = new System.Windows.Forms.ToolStripSeparator();
            this.Sliding_Puzzle_Exit_Game_MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sliding_Puzzle_ToolStrip = new System.Windows.Forms.ToolStrip();
            this.Sliding_Puzzle_Time_Label = new System.Windows.Forms.ToolStripLabel();
            this.Sliding_Puzzle_StopWatch_Label = new System.Windows.Forms.ToolStripLabel();
            this.Sliding_Puzzle_Moves_Label = new System.Windows.Forms.ToolStripLabel();
            this.Sliding_Puzzle_Counter_Label = new System.Windows.Forms.ToolStripLabel();
            this.Sliding_Puzzle_Panel = new System.Windows.Forms.Panel();
            this.Sliding_Puzzle_OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.Sliding_Puzzle_Timer = new System.Windows.Forms.Timer(this.components);
            this.Sliding_Puzzle_MenuStrip.SuspendLayout();
            this.Sliding_Puzzle_ToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // Sliding_Puzzle_MenuStrip
            // 
            this.Sliding_Puzzle_MenuStrip.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sliding_Puzzle_MenuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.Sliding_Puzzle_MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Sliding_Puzzle_Game_Menu_MenuItem});
            this.Sliding_Puzzle_MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.Sliding_Puzzle_MenuStrip.Name = "Sliding_Puzzle_MenuStrip";
            this.Sliding_Puzzle_MenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.Sliding_Puzzle_MenuStrip.Size = new System.Drawing.Size(316, 36);
            this.Sliding_Puzzle_MenuStrip.TabIndex = 0;
            this.Sliding_Puzzle_MenuStrip.Text = "Sliding_Puzzle_MenuStrip";
            // 
            // Sliding_Puzzle_Game_Menu_MenuItem
            // 
            this.Sliding_Puzzle_Game_Menu_MenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Sliding_Puzzle_New_Game_MenuItem,
            this.Sliding_Puzzle_1_Separator,
            this.Sliding_Puzzle_Pause_Game_MenuItem,
            this.Sliding_Puzzle_Continue_Game_MenuItem,
            this.Sliding_Puzzle_2_Separator,
            this.Sliding_Puzzle_Board_Size_MenuItem,
            this.Sliding_Puzzle_Scramble_MenuItem,
            this.Sliding_Puzzle_3_Separator,
            this.Sliding_Puzzle_Exit_Game_MenuItem});
            this.Sliding_Puzzle_Game_Menu_MenuItem.Name = "Sliding_Puzzle_Game_Menu_MenuItem";
            this.Sliding_Puzzle_Game_Menu_MenuItem.Size = new System.Drawing.Size(130, 32);
            this.Sliding_Puzzle_Game_Menu_MenuItem.Text = "Game Menu";
            // 
            // Sliding_Puzzle_New_Game_MenuItem
            // 
            this.Sliding_Puzzle_New_Game_MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("Sliding_Puzzle_New_Game_MenuItem.Image")));
            this.Sliding_Puzzle_New_Game_MenuItem.Name = "Sliding_Puzzle_New_Game_MenuItem";
            this.Sliding_Puzzle_New_Game_MenuItem.Size = new System.Drawing.Size(252, 32);
            this.Sliding_Puzzle_New_Game_MenuItem.Text = "New Game";
            this.Sliding_Puzzle_New_Game_MenuItem.Click += new System.EventHandler(this.Sliding_Puzzle_New_Game_MenuItem_Click);
            // 
            // Sliding_Puzzle_1_Separator
            // 
            this.Sliding_Puzzle_1_Separator.Name = "Sliding_Puzzle_1_Separator";
            this.Sliding_Puzzle_1_Separator.Size = new System.Drawing.Size(249, 6);
            // 
            // Sliding_Puzzle_Pause_Game_MenuItem
            // 
            this.Sliding_Puzzle_Pause_Game_MenuItem.Enabled = false;
            this.Sliding_Puzzle_Pause_Game_MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("Sliding_Puzzle_Pause_Game_MenuItem.Image")));
            this.Sliding_Puzzle_Pause_Game_MenuItem.Name = "Sliding_Puzzle_Pause_Game_MenuItem";
            this.Sliding_Puzzle_Pause_Game_MenuItem.Size = new System.Drawing.Size(252, 32);
            this.Sliding_Puzzle_Pause_Game_MenuItem.Text = "Pause Game";
            this.Sliding_Puzzle_Pause_Game_MenuItem.Click += new System.EventHandler(this.Sliding_Puzzle_Pause_Game_MenuItem_Click);
            // 
            // Sliding_Puzzle_Continue_Game_MenuItem
            // 
            this.Sliding_Puzzle_Continue_Game_MenuItem.Enabled = false;
            this.Sliding_Puzzle_Continue_Game_MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("Sliding_Puzzle_Continue_Game_MenuItem.Image")));
            this.Sliding_Puzzle_Continue_Game_MenuItem.Name = "Sliding_Puzzle_Continue_Game_MenuItem";
            this.Sliding_Puzzle_Continue_Game_MenuItem.Size = new System.Drawing.Size(252, 32);
            this.Sliding_Puzzle_Continue_Game_MenuItem.Text = "Continue Game";
            this.Sliding_Puzzle_Continue_Game_MenuItem.Click += new System.EventHandler(this.Sliding_Puzzle_Continue_Game_MenuItem_Click);
            // 
            // Sliding_Puzzle_2_Separator
            // 
            this.Sliding_Puzzle_2_Separator.Name = "Sliding_Puzzle_2_Separator";
            this.Sliding_Puzzle_2_Separator.Size = new System.Drawing.Size(249, 6);
            // 
            // Sliding_Puzzle_Board_Size_MenuItem
            // 
            this.Sliding_Puzzle_Board_Size_MenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Sliding_Puzzle_Small_Size_MenuItem,
            this.Sliding_Puzzle_Medium_Size_MenuItem,
            this.Sliding_Puzzle_Large_Size_MenuItem});
            this.Sliding_Puzzle_Board_Size_MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("Sliding_Puzzle_Board_Size_MenuItem.Image")));
            this.Sliding_Puzzle_Board_Size_MenuItem.Name = "Sliding_Puzzle_Board_Size_MenuItem";
            this.Sliding_Puzzle_Board_Size_MenuItem.Size = new System.Drawing.Size(252, 32);
            this.Sliding_Puzzle_Board_Size_MenuItem.Text = "Board Size";
            // 
            // Sliding_Puzzle_Small_Size_MenuItem
            // 
            this.Sliding_Puzzle_Small_Size_MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("Sliding_Puzzle_Small_Size_MenuItem.Image")));
            this.Sliding_Puzzle_Small_Size_MenuItem.Name = "Sliding_Puzzle_Small_Size_MenuItem";
            this.Sliding_Puzzle_Small_Size_MenuItem.Size = new System.Drawing.Size(127, 32);
            this.Sliding_Puzzle_Small_Size_MenuItem.Text = "3x3";
            this.Sliding_Puzzle_Small_Size_MenuItem.Click += new System.EventHandler(this.Sliding_Puzzle_Small_Size_MenuItem_Click);
            // 
            // Sliding_Puzzle_Medium_Size_MenuItem
            // 
            this.Sliding_Puzzle_Medium_Size_MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("Sliding_Puzzle_Medium_Size_MenuItem.Image")));
            this.Sliding_Puzzle_Medium_Size_MenuItem.Name = "Sliding_Puzzle_Medium_Size_MenuItem";
            this.Sliding_Puzzle_Medium_Size_MenuItem.Size = new System.Drawing.Size(127, 32);
            this.Sliding_Puzzle_Medium_Size_MenuItem.Text = "4x4";
            this.Sliding_Puzzle_Medium_Size_MenuItem.Click += new System.EventHandler(this.Sliding_Puzzle_Medium_Size_MenuItem_Click);
            // 
            // Sliding_Puzzle_Large_Size_MenuItem
            // 
            this.Sliding_Puzzle_Large_Size_MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("Sliding_Puzzle_Large_Size_MenuItem.Image")));
            this.Sliding_Puzzle_Large_Size_MenuItem.Name = "Sliding_Puzzle_Large_Size_MenuItem";
            this.Sliding_Puzzle_Large_Size_MenuItem.Size = new System.Drawing.Size(127, 32);
            this.Sliding_Puzzle_Large_Size_MenuItem.Text = "5x5";
            this.Sliding_Puzzle_Large_Size_MenuItem.Click += new System.EventHandler(this.Sliding_Puzzle_Large_Size_MenuItem_Click);
            // 
            // Sliding_Puzzle_Scramble_MenuItem
            // 
            this.Sliding_Puzzle_Scramble_MenuItem.Enabled = false;
            this.Sliding_Puzzle_Scramble_MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("Sliding_Puzzle_Scramble_MenuItem.Image")));
            this.Sliding_Puzzle_Scramble_MenuItem.Name = "Sliding_Puzzle_Scramble_MenuItem";
            this.Sliding_Puzzle_Scramble_MenuItem.Size = new System.Drawing.Size(252, 32);
            this.Sliding_Puzzle_Scramble_MenuItem.Text = "Scramble";
            this.Sliding_Puzzle_Scramble_MenuItem.Click += new System.EventHandler(this.Sliding_Puzzle_Scramble_MenuItem_Click);
            // 
            // Sliding_Puzzle_3_Separator
            // 
            this.Sliding_Puzzle_3_Separator.Name = "Sliding_Puzzle_3_Separator";
            this.Sliding_Puzzle_3_Separator.Size = new System.Drawing.Size(249, 6);
            // 
            // Sliding_Puzzle_Exit_Game_MenuItem
            // 
            this.Sliding_Puzzle_Exit_Game_MenuItem.Image = ((System.Drawing.Image)(resources.GetObject("Sliding_Puzzle_Exit_Game_MenuItem.Image")));
            this.Sliding_Puzzle_Exit_Game_MenuItem.Name = "Sliding_Puzzle_Exit_Game_MenuItem";
            this.Sliding_Puzzle_Exit_Game_MenuItem.Size = new System.Drawing.Size(252, 32);
            this.Sliding_Puzzle_Exit_Game_MenuItem.Text = "Exit Game";
            this.Sliding_Puzzle_Exit_Game_MenuItem.Click += new System.EventHandler(this.Sliding_Puzzle_Exit_Game_MenuItem_Click);
            // 
            // Sliding_Puzzle_ToolStrip
            // 
            this.Sliding_Puzzle_ToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Sliding_Puzzle_ToolStrip.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sliding_Puzzle_ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.Sliding_Puzzle_ToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.Sliding_Puzzle_ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Sliding_Puzzle_Time_Label,
            this.Sliding_Puzzle_StopWatch_Label,
            this.Sliding_Puzzle_Moves_Label,
            this.Sliding_Puzzle_Counter_Label});
            this.Sliding_Puzzle_ToolStrip.Location = new System.Drawing.Point(0, 353);
            this.Sliding_Puzzle_ToolStrip.Name = "Sliding_Puzzle_ToolStrip";
            this.Sliding_Puzzle_ToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.Sliding_Puzzle_ToolStrip.Size = new System.Drawing.Size(316, 31);
            this.Sliding_Puzzle_ToolStrip.TabIndex = 1;
            this.Sliding_Puzzle_ToolStrip.Text = "Sliding_Puzzle_ToolStrip";
            // 
            // Sliding_Puzzle_Time_Label
            // 
            this.Sliding_Puzzle_Time_Label.Name = "Sliding_Puzzle_Time_Label";
            this.Sliding_Puzzle_Time_Label.Size = new System.Drawing.Size(58, 28);
            this.Sliding_Puzzle_Time_Label.Text = "Time:";
            // 
            // Sliding_Puzzle_StopWatch_Label
            // 
            this.Sliding_Puzzle_StopWatch_Label.Name = "Sliding_Puzzle_StopWatch_Label";
            this.Sliding_Puzzle_StopWatch_Label.Size = new System.Drawing.Size(60, 28);
            this.Sliding_Puzzle_StopWatch_Label.Text = "00:00";
            // 
            // Sliding_Puzzle_Moves_Label
            // 
            this.Sliding_Puzzle_Moves_Label.Name = "Sliding_Puzzle_Moves_Label";
            this.Sliding_Puzzle_Moves_Label.Size = new System.Drawing.Size(74, 28);
            this.Sliding_Puzzle_Moves_Label.Text = "Moves:";
            // 
            // Sliding_Puzzle_Counter_Label
            // 
            this.Sliding_Puzzle_Counter_Label.Name = "Sliding_Puzzle_Counter_Label";
            this.Sliding_Puzzle_Counter_Label.Size = new System.Drawing.Size(23, 28);
            this.Sliding_Puzzle_Counter_Label.Text = "0";
            // 
            // Sliding_Puzzle_Panel
            // 
            this.Sliding_Puzzle_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Sliding_Puzzle_Panel.Enabled = false;
            this.Sliding_Puzzle_Panel.Location = new System.Drawing.Point(0, 36);
            this.Sliding_Puzzle_Panel.Name = "Sliding_Puzzle_Panel";
            this.Sliding_Puzzle_Panel.Size = new System.Drawing.Size(316, 317);
            this.Sliding_Puzzle_Panel.TabIndex = 2;
            // 
            // Sliding_Puzzle_OpenFileDialog
            // 
            this.Sliding_Puzzle_OpenFileDialog.Filter = "\"image file (*.jpg, *.bmp, *.png) | *.jpg; *.bmp; *.png| all files (*.*) | *.* \"";
            this.Sliding_Puzzle_OpenFileDialog.Title = "Select Image To Shuffle";
            // 
            // Sliding_Puzzle_Timer
            // 
            this.Sliding_Puzzle_Timer.Tick += new System.EventHandler(this.Sliding_Puzzle_Timer_Tick);
            // 
            // Sliding_Puzzle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 384);
            this.Controls.Add(this.Sliding_Puzzle_Panel);
            this.Controls.Add(this.Sliding_Puzzle_ToolStrip);
            this.Controls.Add(this.Sliding_Puzzle_MenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.Sliding_Puzzle_MenuStrip;
            this.MaximizeBox = false;
            this.Name = "Sliding_Puzzle";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sliding Puzzle";
            this.Sliding_Puzzle_MenuStrip.ResumeLayout(false);
            this.Sliding_Puzzle_MenuStrip.PerformLayout();
            this.Sliding_Puzzle_ToolStrip.ResumeLayout(false);
            this.Sliding_Puzzle_ToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip Sliding_Puzzle_MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_Game_Menu_MenuItem;
        private System.Windows.Forms.ToolStrip Sliding_Puzzle_ToolStrip;
        private System.Windows.Forms.ToolStripLabel Sliding_Puzzle_Time_Label;
        private System.Windows.Forms.ToolStripLabel Sliding_Puzzle_StopWatch_Label;
        private System.Windows.Forms.Panel Sliding_Puzzle_Panel;
        private System.Windows.Forms.ToolStripLabel Sliding_Puzzle_Moves_Label;
        private System.Windows.Forms.ToolStripLabel Sliding_Puzzle_Counter_Label;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_New_Game_MenuItem;
        private System.Windows.Forms.ToolStripSeparator Sliding_Puzzle_1_Separator;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_Pause_Game_MenuItem;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_Continue_Game_MenuItem;
        private System.Windows.Forms.ToolStripSeparator Sliding_Puzzle_2_Separator;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_Board_Size_MenuItem;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_Small_Size_MenuItem;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_Medium_Size_MenuItem;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_Large_Size_MenuItem;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_Scramble_MenuItem;
        private System.Windows.Forms.ToolStripSeparator Sliding_Puzzle_3_Separator;
        private System.Windows.Forms.ToolStripMenuItem Sliding_Puzzle_Exit_Game_MenuItem;
        private System.Windows.Forms.OpenFileDialog Sliding_Puzzle_OpenFileDialog;
        private System.Windows.Forms.Timer Sliding_Puzzle_Timer;
    }
}

