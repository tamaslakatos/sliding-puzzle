﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sliding_puzzle
{
    public partial class Sliding_Puzzle : Form
    {
        private enum Direction
        {
            Top,
            Bottom,
            Left,
            Right
        };

        private enum GameState
        {
            Started,
            Scrambled,
            Finished
        };

        #region Global Variables

        private int ShuffleTime = 3000;
        private int BoardSize = 3;
        private int Counter = 0;

        private Image Picture;
        private GameState State;
        private PictureBox PuzzlePiece;
        private Stopwatch Ticker = new Stopwatch();

        #endregion

        public Sliding_Puzzle()
        {
            InitializeComponent();

            Size = new Size(229, 304);
            Sliding_Puzzle_ToolStrip.Hide();

            MainMenuStrip.Renderer = new CustomMenuStripProfessionalRenderer();
            Sliding_Puzzle_ToolStrip.Renderer = new CustomToolStripProfessionalRenderer();
            Sliding_Puzzle_Panel.BackColor = Color.FromArgb(29, 29, 29);
        }

        #region MenuStrip

        private void Sliding_Puzzle_New_Game_MenuItem_Click(object sender, EventArgs e)
        {

            if (Sliding_Puzzle_OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (Sliding_Puzzle_ToolStrip.Visible != true) { Sliding_Puzzle_ToolStrip.Show(); }
                if (Sliding_Puzzle_Panel.Controls.Count != 0) { Sliding_Puzzle_Panel.Controls.Clear(); }

                Picture = new Bitmap(Image.FromFile(Sliding_Puzzle_OpenFileDialog.FileName),
                    new Size(Sliding_Puzzle_Panel.Width, Sliding_Puzzle_Panel.Height));

                CreateBoard();

                Sliding_Puzzle_Scramble_MenuItem.Enabled = true;

                if (State == GameState.Scrambled || State == GameState.Finished)
                {
                    Counter = 0;
                    Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                    Ticker.Reset();

                    if (State == GameState.Scrambled) { Sliding_Puzzle_Pause_Game_MenuItem.Enabled = false; KeyDown -= Sliding_Puzzle_Piece_Button; Sliding_Puzzle_Continue_Game_MenuItem.Enabled = false; }
                }

                State = GameState.Started;
            }
        }

        private void Sliding_Puzzle_Pause_Game_MenuItem_Click(object sender, EventArgs e)
        {
            KeyDown -= Sliding_Puzzle_Piece_Button;

            Sliding_Puzzle_Panel.Enabled = false;
            Ticker.Stop();

            Sliding_Puzzle_Pause_Game_MenuItem.Enabled = false;
            Sliding_Puzzle_Continue_Game_MenuItem.Enabled = true;
        }

        private void Sliding_Puzzle_Continue_Game_MenuItem_Click(object sender, EventArgs e)
        {
            KeyDown += Sliding_Puzzle_Piece_Button;

            Sliding_Puzzle_Panel.Enabled = true;
            Ticker.Start();

            Sliding_Puzzle_Pause_Game_MenuItem.Enabled = true;
            Sliding_Puzzle_Continue_Game_MenuItem.Enabled = false;
        }

        private void Sliding_Puzzle_Small_Size_MenuItem_Click(object sender, EventArgs e)
        {
            Size = new Size(229, 304);
            BoardSize = 3;
            ShuffleTime = 3000;

            if (Sliding_Puzzle_Panel.Controls.Count != 0)
            {
                KeyDown -= Sliding_Puzzle_Piece_Button;

                Sliding_Puzzle_Panel.Controls.Clear();

                Picture = new Bitmap(Image.FromFile(Sliding_Puzzle_OpenFileDialog.FileName),
                    new Size(Sliding_Puzzle_Panel.Width, Sliding_Puzzle_Panel.Height));

                CreateBoard();

                if (State == GameState.Scrambled || State == GameState.Finished)
                {
                    Counter = 0;
                    Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                    Ticker.Reset();

                    if (State == GameState.Scrambled)
                    {
                        Sliding_Puzzle_Panel.Enabled = false;
                        Sliding_Puzzle_Pause_Game_MenuItem.Enabled = false;
                        Sliding_Puzzle_Continue_Game_MenuItem.Enabled = false;
                        Sliding_Puzzle_Scramble_MenuItem.Enabled = true;
                    }
                }
            }
        }

        private void Sliding_Puzzle_Medium_Size_MenuItem_Click(object sender, EventArgs e)
        {
            Size = new Size(301, 373);
            BoardSize = 4;
            ShuffleTime = 4000;

            if (Sliding_Puzzle_Panel.Controls.Count != 0)
            {
                KeyDown -= Sliding_Puzzle_Piece_Button;

                Sliding_Puzzle_Panel.Controls.Clear();

                Picture = new Bitmap(Image.FromFile(Sliding_Puzzle_OpenFileDialog.FileName),
                    new Size(Sliding_Puzzle_Panel.Width, Sliding_Puzzle_Panel.Height));

                CreateBoard();

                if (State == GameState.Scrambled || State == GameState.Finished)
                {
                    Counter = 0;
                    Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                    Ticker.Reset();

                    if (State == GameState.Scrambled)
                    {
                        Sliding_Puzzle_Panel.Enabled = false;
                        Sliding_Puzzle_Pause_Game_MenuItem.Enabled = false;
                        Sliding_Puzzle_Continue_Game_MenuItem.Enabled = false;
                        Sliding_Puzzle_Scramble_MenuItem.Enabled = true;
                    }
                }
            }
        }

        private void Sliding_Puzzle_Large_Size_MenuItem_Click(object sender, EventArgs e)
        {
            Size = new Size(373, 444);
            BoardSize = 5;
            ShuffleTime = 5000;

            if (Sliding_Puzzle_Panel.Controls.Count != 0)
            {
                KeyDown -= Sliding_Puzzle_Piece_Button;

                Sliding_Puzzle_Panel.Controls.Clear();

                Picture = new Bitmap(Image.FromFile(Sliding_Puzzle_OpenFileDialog.FileName),
                    new Size(Sliding_Puzzle_Panel.Width, Sliding_Puzzle_Panel.Height));

                CreateBoard();

                if (State == GameState.Scrambled || State == GameState.Finished)
                {
                    Counter = 0;
                    Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                    Ticker.Reset();

                    if (State == GameState.Scrambled)
                    {
                        Sliding_Puzzle_Panel.Enabled = false;
                        Sliding_Puzzle_Pause_Game_MenuItem.Enabled = false;
                        Sliding_Puzzle_Continue_Game_MenuItem.Enabled = false;
                        Sliding_Puzzle_Scramble_MenuItem.Enabled = true;
                    }
                }
            }
        }

        private async void Sliding_Puzzle_Scramble_MenuItem_Click(object sender, EventArgs e)
        {
            if (State == GameState.Finished)
            {
                Counter = 0;
                Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                Ticker.Reset();
            }

            Sliding_Puzzle_New_Game_MenuItem.Enabled = false;
            Sliding_Puzzle_Board_Size_MenuItem.Enabled = false;
            Sliding_Puzzle_Scramble_MenuItem.Enabled = false;

            PuzzlePiece = (PictureBox)Sliding_Puzzle_Panel.Controls[Sliding_Puzzle_Panel.Controls.Count - 1];
            Sliding_Puzzle_Panel.Controls.Remove(PuzzlePiece);

            MinimizeBox = false;
            Stopwatch sw = Stopwatch.StartNew();
            await Task.Run(() => ShuffleBoard(new Rectangle(PuzzlePiece.Location, PuzzlePiece.Size), sw));
            MinimizeBox = true;

            Sliding_Puzzle_Panel.Enabled = true;
            Sliding_Puzzle_New_Game_MenuItem.Enabled = true;
            Sliding_Puzzle_Pause_Game_MenuItem.Enabled = true;
            Sliding_Puzzle_Board_Size_MenuItem.Enabled = true;

            KeyDown += Sliding_Puzzle_Piece_Button;

            Sliding_Puzzle_Timer.Enabled = true;
            Ticker.Start();

            State = GameState.Scrambled;
        }

        private void Sliding_Puzzle_Exit_Game_MenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region Event Methods

        private void Sliding_Puzzle_Piece_Click(object sender, MouseEventArgs e)
        {
            PictureBox selectedPuzzlePiece = (PictureBox)sender;

            Dictionary<Direction, dynamic> puzzlePieceBounds = new Dictionary<Direction, dynamic>()
            {
                { Direction.Top, selectedPuzzlePiece.Top },
                { Direction.Bottom, selectedPuzzlePiece.Bottom },
                { Direction.Left, selectedPuzzlePiece.Left },
                { Direction.Right, selectedPuzzlePiece.Right }
            };

            foreach (KeyValuePair<Direction, dynamic> bound in puzzlePieceBounds.ToList())
            {
                if (!CheckBoundaryWithPanel(bound.Value) && !CheckCollisionWithPuzzlePiece(bound.Key, selectedPuzzlePiece))
                {
                    puzzlePieceBounds[bound.Key] = true;
                }
                else
                {
                    puzzlePieceBounds[bound.Key] = false;
                }
            }

            foreach (KeyValuePair<Direction, dynamic> bound in puzzlePieceBounds.ToList())
            {
                if (bound.Value)
                {
                    MovePuzzlePiece(bound.Key, selectedPuzzlePiece);

                    Counter++;
                    Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                }
            }

            if (IfSolved())
            {
                KeyDown -= Sliding_Puzzle_Piece_Button;

                Sliding_Puzzle_Panel.Enabled = false;
                Sliding_Puzzle_Panel.Controls.Add(PuzzlePiece);

                Ticker.Stop();

                Sliding_Puzzle_Pause_Game_MenuItem.Enabled = false;
                Sliding_Puzzle_Scramble_MenuItem.Enabled = true;

                State = GameState.Finished;
            }
        }

        private void Sliding_Puzzle_Piece_Button(Object o, KeyEventArgs e)
        {
            Dictionary<Direction, dynamic> puzzlePieceBounds = new Dictionary<Direction, dynamic>()
            {
                { Direction.Top, PuzzlePiece.Top },
                { Direction.Bottom, PuzzlePiece.Bottom },
                { Direction.Left, PuzzlePiece.Left },
                { Direction.Right, PuzzlePiece.Right }
            };

            foreach (KeyValuePair<Direction, dynamic> bound in puzzlePieceBounds.ToList())
            {
                if (!CheckBoundaryWithPanel(bound.Value) && CheckCollisionWithPuzzlePiece(bound.Key, PuzzlePiece))
                {
                    puzzlePieceBounds[bound.Key] = true;
                }
                else
                {
                    puzzlePieceBounds[bound.Key] = false;
                }
            }

            switch (e.KeyCode)
            {
                case Keys.Up:

                    if (puzzlePieceBounds[Direction.Bottom] != false)
                    {
                        PictureBox puzzlePieceUp = Sliding_Puzzle_Panel.Controls.OfType<PictureBox>()
                            .Where(x => x.Tag.ToString() == ((int)PuzzlePiece.Tag + BoardSize).ToString())
                            .ToList()[0];

                        MovePuzzlePiece(Direction.Top, puzzlePieceUp);

                        Counter++;
                        Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                    }

                    break;

                case Keys.Down:

                    if (puzzlePieceBounds[Direction.Top] != false)
                    {
                        PictureBox puzzlePieceDown = Sliding_Puzzle_Panel.Controls.OfType<PictureBox>()
                        .Where(x => x.Tag.ToString() == ((int)PuzzlePiece.Tag - BoardSize).ToString()).ToList()[0];

                        MovePuzzlePiece(Direction.Bottom, puzzlePieceDown);

                        Counter++;
                        Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                    }

                    break;

                case Keys.Left:

                    if (puzzlePieceBounds[Direction.Right] != false)
                    {
                        PictureBox puzzlePieceLeft = Sliding_Puzzle_Panel.Controls.OfType<PictureBox>()
                        .Where(x => x.Tag.ToString() == ((int)PuzzlePiece.Tag + 1).ToString()).ToList()[0];

                        MovePuzzlePiece(Direction.Left, puzzlePieceLeft);

                        Counter++;
                        Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                    }

                    break;

                case Keys.Right:

                    if (puzzlePieceBounds[Direction.Left] != false)
                    {
                        PictureBox puzzlePieceRight = Sliding_Puzzle_Panel.Controls.OfType<PictureBox>()
                            .Where(x => x.Tag.ToString() == ((int)PuzzlePiece.Tag - 1).ToString()).ToList()[0];

                        MovePuzzlePiece(Direction.Right, puzzlePieceRight);

                        Counter++;
                        Sliding_Puzzle_Counter_Label.Text = Counter.ToString();
                    }

                    break;

                default:
                    break;
            }

            if (IfSolved())
            {
                KeyDown -= Sliding_Puzzle_Piece_Button;

                Sliding_Puzzle_Panel.Enabled = false;
                Sliding_Puzzle_Panel.Controls.Add(PuzzlePiece);

                Ticker.Stop();

                Sliding_Puzzle_Pause_Game_MenuItem.Enabled = false;
                Sliding_Puzzle_Scramble_MenuItem.Enabled = true;

                State = GameState.Finished;
            }
        }

        private void Sliding_Puzzle_Timer_Tick(object sender, EventArgs e)
        {
            int minutes = Ticker.Elapsed.Minutes;
            int seconds = Ticker.Elapsed.Seconds;

            switch (minutes)
            {
                case int n when n < 10:
                    {
                        switch (minutes)
                        {
                            case int m when m < 1:
                                {
                                    if (seconds < 10)
                                        Sliding_Puzzle_StopWatch_Label.Text = "00:0" + seconds;
                                    else
                                        Sliding_Puzzle_StopWatch_Label.Text = "00:" + seconds;

                                    break;
                                }

                            case int m when m >= 1:
                                {
                                    if (seconds < 10)
                                        Sliding_Puzzle_StopWatch_Label.Text = "0" + minutes + ":0" + seconds;
                                    else
                                        Sliding_Puzzle_StopWatch_Label.Text = "0" + minutes + ":" + seconds;

                                    break;
                                }
                        }

                        break;
                    }

                case int n when n > 10:
                    {
                        if (seconds < 10)
                            Sliding_Puzzle_StopWatch_Label.Text = minutes + ":0" + seconds;
                        else
                            Sliding_Puzzle_StopWatch_Label.Text = minutes + ":" + seconds;

                        break;
                    }
            }
        }

        #endregion

        private Bitmap CropImage(Image image, Point location, Size size)
        {
            Bitmap croppedBitmap = new Bitmap(size.Width, size.Height);

            Graphics graphicsDeviceInterface = Graphics.FromImage(croppedBitmap);

            graphicsDeviceInterface.DrawImage(image, new Rectangle(new Point(0, 0), size), new Rectangle(location, size), GraphicsUnit.Pixel);

            return croppedBitmap;
        }

        private void CreateBoard()
        {
            int controlTagNumber = 0;

            for (int i = 0; i < BoardSize; i++)
            {
                for (int j = 0; j < BoardSize; j++)
                {
                    int width = Sliding_Puzzle_Panel.Width / BoardSize;
                    int height = Sliding_Puzzle_Panel.Height / BoardSize;

                    PictureBox slidingPuzzlePiece = new PictureBox()
                    {
                        Size = new Size(71, 71),
                        Location = new Point(72 * j, 72 * i),
                        Tag = controlTagNumber++,
                        BackgroundImage = CropImage(Picture, new Point(j * width, i * height), new Size(width, height))
                    };

                    slidingPuzzlePiece.MouseClick += new MouseEventHandler(Sliding_Puzzle_Piece_Click);

                    Sliding_Puzzle_Panel.Controls.Add(slidingPuzzlePiece);
                }
            }
        }

        private void MovePuzzlePiece(Direction direction, PictureBox puzzlePiece)
        {
            PuzzlePiece.Location = puzzlePiece.Location;
            PuzzlePiece.Tag = puzzlePiece.Tag;

            switch (direction)
            {
                case Direction.Top:

                    for (int i = 0; i < 72; i++)
                    {
                        puzzlePiece.Location
                            = new Point(puzzlePiece.Location.X, puzzlePiece.Location.Y - 1);
                    }

                    puzzlePiece.Tag = (int)puzzlePiece.Tag - BoardSize;

                    break;

                case Direction.Bottom:

                    for (int i = 0; i < 72; i++)
                    {
                        puzzlePiece.Location
                            = new Point(puzzlePiece.Location.X, puzzlePiece.Location.Y + 1);
                    }

                    puzzlePiece.Tag = (int)puzzlePiece.Tag + BoardSize;

                    break;

                case Direction.Left:

                    for (int i = 0; i < 72; i++)
                    {
                        puzzlePiece.Location
                            = new Point(puzzlePiece.Location.X - 1, puzzlePiece.Location.Y);
                    }

                    puzzlePiece.Tag = (int)puzzlePiece.Tag - 1;

                    break;

                case Direction.Right:

                    for (int i = 0; i < 72; i++)
                    {
                        puzzlePiece.Location
                            = new Point(puzzlePiece.Location.X + 1, puzzlePiece.Location.Y);
                    }

                    puzzlePiece.Tag = (int)puzzlePiece.Tag + 1;

                    break;
            }
        }

        private void ShuffleBoard(Rectangle emptySpace, Stopwatch condition)
        {
            Dictionary<Direction, dynamic> puzzlePieceBounds = new Dictionary<Direction, dynamic>()
            {
                    { Direction.Top, emptySpace.Top }, { Direction.Bottom, emptySpace.Bottom },
                    { Direction.Left, emptySpace.Left }, { Direction.Right, emptySpace.Right }
            };

            foreach (KeyValuePair<Direction, dynamic> bound in puzzlePieceBounds.ToList())
            {
                if (!CheckBoundaryWithPanel(bound.Value))
                {
                    puzzlePieceBounds[bound.Key] = true;
                }
                else
                {
                    puzzlePieceBounds[bound.Key] = false;
                }
            }

            int randomizer = new Random()
                     .Next(puzzlePieceBounds.Where(v => v.Value == true).ToList().Count);

            KeyValuePair<Direction, dynamic> filtered = puzzlePieceBounds.Where(c => c.Value == true).ToList()[randomizer];

            PictureBox pictureBoxToMove = new PictureBox();

            Rectangle rectangleCoordinates = new Rectangle();

            switch (filtered.Key)
            {
                case Direction.Top:

                    Sliding_Puzzle_Panel.Invoke(new MethodInvoker(delegate
                    {
                        pictureBoxToMove = (PictureBox)Sliding_Puzzle_Panel.GetChildAtPoint(new Point(emptySpace.Location.X, emptySpace.Location.Y - 72));
                    }));

                    rectangleCoordinates.Location = pictureBoxToMove.Location;
                    rectangleCoordinates.Size = pictureBoxToMove.Size;

                    Invoke((MethodInvoker)delegate
                    {
                        MovePuzzlePiece(Direction.Bottom, pictureBoxToMove);
                    });

                    break;

                case Direction.Bottom:

                    Sliding_Puzzle_Panel.Invoke(new MethodInvoker(delegate
                    {
                        pictureBoxToMove = (PictureBox)Sliding_Puzzle_Panel.GetChildAtPoint(new Point(emptySpace.Location.X, emptySpace.Location.Y + 72));
                    }));

                    rectangleCoordinates.Location = pictureBoxToMove.Location;
                    rectangleCoordinates.Size = pictureBoxToMove.Size;

                    Invoke((MethodInvoker)delegate
                    {
                        MovePuzzlePiece(Direction.Top, pictureBoxToMove);
                    });

                    break;

                case Direction.Left:

                    Sliding_Puzzle_Panel.Invoke(new MethodInvoker(delegate
                    {
                        pictureBoxToMove = (PictureBox)Sliding_Puzzle_Panel.GetChildAtPoint(new Point(emptySpace.Location.X - 72, emptySpace.Location.Y));
                    }));

                    rectangleCoordinates.Location = pictureBoxToMove.Location;
                    rectangleCoordinates.Size = pictureBoxToMove.Size;

                    Invoke((MethodInvoker)delegate
                    {
                        MovePuzzlePiece(Direction.Right, pictureBoxToMove);
                    });

                    break;

                case Direction.Right:

                    Sliding_Puzzle_Panel.Invoke(new MethodInvoker(delegate
                    {
                        pictureBoxToMove = (PictureBox)Sliding_Puzzle_Panel.GetChildAtPoint(new Point(emptySpace.Location.X + 72, emptySpace.Location.Y));
                    }));

                    rectangleCoordinates.Location = pictureBoxToMove.Location;
                    rectangleCoordinates.Size = pictureBoxToMove.Size;

                    Invoke((MethodInvoker)delegate
                    {
                        MovePuzzlePiece(Direction.Left, pictureBoxToMove);
                    });

                    break;
            }

            Thread.Sleep(10);

            if (condition.ElapsedMilliseconds < ShuffleTime)
            {
                ShuffleBoard(rectangleCoordinates, condition);
            }
        }

        private bool CheckBoundaryWithPanel(int bound)
        {
            if (bound == Sliding_Puzzle_Panel.Top - 24 || bound == Sliding_Puzzle_Panel.Bottom + 2
                || bound == Sliding_Puzzle_Panel.Left || bound == Sliding_Puzzle_Panel.Right + 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckCollisionWithPuzzlePiece(Direction direction, PictureBox puzzlePiece)
        {
            switch (direction)
            {
                case Direction.Top:

                    if (Sliding_Puzzle_Panel.GetChildAtPoint(
                        new Point(puzzlePiece.Location.X, puzzlePiece.Location.Y - 72)) != null)
                    { return true; }

                    break;

                case Direction.Bottom:

                    if (Sliding_Puzzle_Panel.GetChildAtPoint(
                       new Point(puzzlePiece.Location.X, puzzlePiece.Location.Y + 72)) != null)
                    { return true; }

                    break;

                case Direction.Left:

                    if (Sliding_Puzzle_Panel.GetChildAtPoint(
                        new Point(puzzlePiece.Location.X - 72, puzzlePiece.Location.Y)) != null)
                    { return true; }

                    break;

                case Direction.Right:

                    if (Sliding_Puzzle_Panel.GetChildAtPoint(
                       new Point(puzzlePiece.Location.X + 72, puzzlePiece.Location.Y)) != null)
                    { return true; }

                    break;
            }

            return false;
        }

        private bool IfSolved()
        {
            int[] numberSequence = new int[Sliding_Puzzle_Panel.Controls.Count];

            for (int i = 0; i < Sliding_Puzzle_Panel.Controls.Count; i++)
            {
                numberSequence[i] = (int)Sliding_Puzzle_Panel.Controls[i].Tag;
            }
            return numberSequence.SequenceEqual(Enumerable.Range(0, numberSequence.Count()));
        }
    }

    class CustomMenuStripProfessionalRenderer : ToolStripProfessionalRenderer
    {
        #region Custom MenuStrip Render Methods

        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
        {
            e.ToolStrip.BackColor = Color.FromArgb(29, 29, 29);
            e.ToolStrip.ForeColor = Color.White;
            e.Graphics.DrawRectangle(new Pen(Color.FromArgb(29, 29, 29)) { Width = 2 }, 1, 1, e.AffectedBounds.Width - 2, e.AffectedBounds.Height - 2);
        }

        protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(29, 29, 29)), new Rectangle(Point.Empty, e.Item.Size));

            if (e.Item.Selected)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(21, 47, 72)), 4, 2, e.Item.Width - 7, e.Item.Height - 4);
                e.Graphics.DrawRectangle(new Pen(Color.FromArgb(7, 84, 161)), 2, 0, new Rectangle(Point.Empty, e.Item.Size).Width - 4, new Rectangle(Point.Empty, e.Item.Size).Height - 1);
            }
        }

        protected override void OnRenderSeparator(ToolStripSeparatorRenderEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(29, 29, 29)), new Rectangle(Point.Empty, e.Item.Size));
            e.Graphics.DrawLine(new Pen(Color.FromArgb(88, 88, 88)), 30, 3, e.Item.Size.Width, e.Item.Size.Height - 3);
        }

        protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
        {
            e.ArrowColor = Color.White; base.OnRenderArrow(e);

            if (!e.Item.Enabled)
            {
                e.ArrowColor = Color.FromArgb(138, 138, 138); base.OnRenderArrow(e);
            }
        }

        #endregion
    }

    class CustomToolStripProfessionalRenderer : ToolStripProfessionalRenderer
    {
        #region Custom MenuStrip Render Methods

        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
        {
            e.ToolStrip.BackColor = Color.FromArgb(29, 29, 29);
            e.ToolStrip.ForeColor = Color.White;
            RoundedEdges = false;
        }

        #endregion
    }
}